package com.mj;

import java.sql.Time;

public class main {
	
	/*
	 * 0 1 1 2 3 5 8 13 .....
	 */
	
	public static int fib1(int n) {
		if (n <= 1) return n;
		return fib1(n-1) + fib1(n-2);
	}
	
	public static int fib2(int n) {
		if (n <= 1) return n;
		
		int first = 0;
		int second = 1;
		for (int i = 0; i < n - 1; i++) {
			int sum = first + second;
			first = second;
			second = sum;
		}
		return second;
	}
	
	public static void main(String[] args) {
		System.out.println(fib2(70));
		//int 类型最大值
		int intMax = 2147483647;
		intMax = intMax + 1;
		System.out.println("int最大值:" + intMax);
		
		int intMin = -2147483648;
		intMin = intMin - 1;
		System.out.println("int值:" + intMin);
		
		
		//操作数
		System.out.println(10 + 2);
		System.out.println(10 - 2);
		System.out.println(10 * 2);
		System.out.println(10 / 2);
		System.out.println("-----------------");
		
		//操作字符
		System.out.println('A' + 'B');
		//操作字符串
		System.out.println("AAA" + "BBB");
		
//		System.out.println(100 / 0);// infinity 正无穷大
//		System.out.println(0.0 / 0.0);//NaN Not a Number 自己都不等于自己
		
		System.out.println(10 / 3);
		System.out.println(9876 / 1000 + 1000);
		System.out.println(9876 / 1000 * 1000);
		System.out.println(10 % 3);

		/*
		 * 前置情况 ++a 表示: a变量自身先加1,在运算;
		 * 后置情况 a++ 表示: a变量自身先加1,把递增1之前的原始值拿去做运算;
		 * */
	
		int a1 = 5;
		int b1 = ++ a1; //a1 = a1 + 1;
		System.out.println("a1=" + a1 + ",b1=" + b1);//a1= 6,b1= 6
		
		System.out.println("========");

		int a2 = 5;
		int b2 = a2++; //a2 = a2(5) + 1;
		System.out.println("a2=" + a2 + ",b2=" + b2);//a2= 6,b2= 5
		
		System.out.println("====================");
		
		int Number3 = 3;
		Number3 = Number3 ++; //Number4++ --> Number4(原始值) + 1; Number4 = 3 (原始值)
		System.out.println(Number3);
		
		System.out.println("========");
		
		int Number4 = 3;
		Number4 = ++ Number4; //++ Number4--> Number4 + 1; Number4 = 4
		System.out.println(Number4);
		

	}
	
	
	
	
	
	
	

}
